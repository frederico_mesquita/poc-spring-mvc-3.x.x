package br.com.papodecafeteria.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.papodecafeteria.dao.jpa.UserDaoJpa;
import br.com.papodecafeteria.dao.model.UserJPA;

@Controller
public class UserController {

	private static Logger l = Logger.getLogger(UserController.class.getName());

	@RequestMapping("/newuser")
	public String addUserForm(){
		return "adduserform";
	}

	@RequestMapping("/add")
	public String addUser(UserJPA pUserJPA){
		int status = 0;
		try{
			status = UserDaoJpa.save(pUserJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		if (status > 0)
			return "redirect:view";
		else
			return "error";
	}

	@RequestMapping("/view")
	public String viewUsers(Model pModel){
		try{
			pModel.addAttribute("lista", UserDaoJpa.getAllRecords());
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return "viewusers";
	}

	@RequestMapping("/edit/{id}")
	public ModelAndView editUser(@PathVariable("id")int pId, Model pModel){
		ModelAndView pModelAndView = null;
		try{
			pModelAndView = new ModelAndView("editform", "userJPA", UserDaoJpa.getRecordById(pId));
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pModelAndView;
	}

	@RequestMapping("/edit/updateUser")
	public String updateUser(UserJPA pUserJPA){
		try{
			UserDaoJpa.update(pUserJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return "redirect:/view";
	}

	@RequestMapping("/delete/{id}")
	public ModelAndView deleteUser(@PathVariable("id")int pId){
		ModelAndView pModelAndView = null;
		try{
			UserJPA pUserJpa = new UserJPA();
			pUserJpa.setId(pId);
			UserDaoJpa.delete(pUserJpa);
			pModelAndView = new ModelAndView("redirect:/view", "lista", UserDaoJpa.getAllRecords());
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pModelAndView;
	}
}
