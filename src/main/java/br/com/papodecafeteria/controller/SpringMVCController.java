package br.com.papodecafeteria.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller	
public class SpringMVCController {
	
	@RequestMapping("/sayHello")
	public String sayHello(Model pModel){
        pModel.addAttribute("persons", getMockLst());
				
		return "springMvcHelloWorld";
	}

	@RequestMapping("/hello")
	public ModelAndView helloWorld(){
		return new ModelAndView("springMvcHelloWorld", "cSayHello", "Spring MVC 3.2.x");
	}
	
	private List<String> getMockLst(){
		List<String> persons = new ArrayList<String>();
        persons.add(new String("Tousif tousif@mail.com"));
        persons.add(new String("Asif asif@mail.com"));
        persons.add(new String("Ramiz ramiz@mail.com"));
        persons.add(new String("Rizwan rizwan@mail.com"));
        persons.add(new String("Amol amol@mail.com"));
        persons.add(new String("Ramdas ramdas@mail.com"));
        return persons;
	}
}
