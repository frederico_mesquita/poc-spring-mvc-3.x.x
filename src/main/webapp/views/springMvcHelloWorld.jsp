<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Spring MVC 3.x.x - Testing Controller</title>
	</head>
	<body>
		<p>Spring MVC 3.x.x - Testing Controller</p>

		<c:forEach var="person" items="${persons}" varStatus="loopCounter">
		<c:out value="${loopCounter.count}" />
		<c:out value="${person}" />
		</c:forEach>
	</body>
</html>