<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Spring MVC 3.x.x - ADD USER</title>
	</head>
	<body>
		<div align="center">
			<h3>Spring MVC 3.x.x - ADD USER</h3>
			<form id="add" name="add" action="add" method="post">
				<table>  
					<tr><td>Name:</td><td><input id="name" type="text" name="name"/></td></tr>  
					<tr><td>Password:</td><td><input id="password" type="password" name="password"/></td></tr>  
					<tr><td>Email:</td><td><input id="email" type="email" name="email"/></td></tr>  
					<tr>
						<td>Sex:</td>
						<td>  
							<input id="male" type="radio" name="sex" value="male"/>Male   
							<input id="female" type="radio" name="sex" value="female"/>Female
						</td>
					</tr>  
					<tr>
						<td>Country:</td>
						<td>
							<select id="country" name="country" style="width:155px">  
								<option>Peru</option>
								<option>Argentina</option>
								<option>Bolivia</option>  
								<option>Uruguay</option>  
								<option>Chile</option>  
								<option selected="selected">Brazil</option>
								<option>Other</option>  
							</select>
						</td>
					</tr>
					<tr align="center">
						<td><input id="add" type="submit" value="Add User"/></td>
						<td><button id="cancel" onclick="location.href='./index.jsp'" type="button">Cancel</button>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</body>
</html>