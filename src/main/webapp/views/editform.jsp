<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Edit Form</title>
	</head>
	<body>
		<div align="center" id="edituser">
			<h1>Edit Form</h1>
			<form id="edituserform" action="updateUser" method="post">  
				<input type="hidden" name="id" value="${userJPA.id}"/>  
				<table>  
					<tr><td>Name:</td><td><input type="text" id="name" name="name" value="${userJPA.name}"/></td></tr>  
					<tr><td>Email:</td><td><input type="email" id="name" name="email" value="${userJPA.email}"/></td></tr>  
					<tr>
						<td>Sex:</td>
						<td>
							<c:set var = "sex" scope = "session" value = "${userJPA.sex}"/>
							<c:if test = "${sex == 'female'}">
								<input type="radio" name="sex" id="female" value="female" checked="checked" />Female
								<input type="radio" name="sex" id="male" value="male" />Male
							</c:if>
							<c:if test = "${sex == 'male' || sex == 'Male'}">
								<input type="radio" name="sex" id="female" value="female" />Female
								<input type="radio" name="sex" id="male" value="male" checked="checked" />Male
							</c:if>
						</td>
					</tr> 
					<tr>
						<td>Country:</td>
						<td>
							<select id="country" name="country" style="width:155px">  
								<c:set var = "country" scope = "session" value = "${userJPA.country}"/>
								<c:if test = "${country == 'Peru'}">
									<option selected="selected">Peru</option>
								</c:if>
								<c:if test = "${country != 'Peru'}">
									<option>Peru</option>
								</c:if>
								<c:if test = "${country == 'Argentina'}">
									<option selected="selected">Argentina</option>
								</c:if>
								<c:if test = "${country != 'Argentina'}">
									<option>Argentina</option>
								</c:if>
								<c:if test = "${country == 'Bolivia'}">
									<option selected="selected">Bolivia</option>
								</c:if>
								<c:if test = "${country != 'Bolivia'}">
									<option>Bolivia</option>
								</c:if>
								<c:if test = "${country == 'Uruguay'}">
									<option selected="selected">Uruguay</option>
								</c:if>
								<c:if test = "${country != 'Uruguay'}">
									<option>Uruguay</option>
								</c:if>
								<c:if test = "${country == 'Chile'}">
									<option selected="selected">Chile</option>
								</c:if>
								<c:if test = "${country != 'Chile'}">
									<option>Chile</option>
								</c:if>
								<c:if test = "${country == 'Brazil'}">
									<option selected="selected">Brazil</option>
								</c:if>
								<c:if test = "${country != 'Brazil'}">
									<option>Brazil</option>
								</c:if>
							</select>
						</td>
					</tr>  
					<tr align="center">
						<td><input id="updateUser" type="submit" value="Save User"/></td>
						<td><button id="cancel" onclick="history.go(-1);" type="button">Cancel</button>
					</tr>
				</table>  
			</form>
		</div>
	</body>
</html>