<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script>
		$(document).ready(function(){
			$(".restore").fadeOut();
		    $(".link").click(function(){
		        $("#home").fadeOut();
		    });
		});
		
		$(document).ready(function(){
			$(".line").click(function(){
		        $(this).slideUp();
		        $(".restore").fadeIn();
		    });
			$(".restore").click(function(){
				$(".line").slideDown();
		        $(".restore").fadeOut();
		    });
		});
		</script>
		<title>View Users</title>
	</head>
	<body>
		<div align="center" id="viewusers">
			<h1>List Users</h1>
			
	<c:if test="${not empty lists}">

		<ul>
			<c:forEach var="listValue" items="${teste}">
				<li>${listValue}</li>
			</c:forEach>
		</ul>

	</c:if>
			
			<table border="1" width="90%" cellpadding="10">
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Email</th>
					<th>Sex</th>
					<th>Country</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
				<c:if test="${not empty lista}">
					<c:forEach items="${lista}" var="usuario">
						<tr id="line" class="line">
							<td align="center">${usuario.id}</td>
							<td id="name">${usuario.name}</td> 
							<td id="email">${usuario.email}</td>
							<td id="sex" align="center">${usuario.sex}</td>
							<td id="country">${usuario.country}</td>
							<td id="edit" align="center"><a href="edit/${usuario.id}">Edit User</a></td>
							<td id="edit" align="center"><a href="delete/${usuario.id}">Delete User</a></td>
						</tr>
					</c:forEach>
				</c:if>
			</table>  
			<br/>
			<p><a href="index.jsp">Home</a></p>
			<p class="restore">Click me to restores table�s lines.</p>
		</div>
	</body>
</html>